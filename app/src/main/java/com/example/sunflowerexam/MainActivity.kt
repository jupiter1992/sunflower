package com.example.sunflowerexam

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.Observer
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import com.example.sunflowerexam.data.Plant
import com.example.sunflowerexam.data.Plants
import com.example.sunflowerexam.database.ObjectBox
import com.example.sunflowerexam.screen.plant.PlantListVM
import com.example.sunflowerexam.task.SeedDatabaseWorker
import com.example.sunflowerexam.utils.Application
import io.objectbox.android.AndroidScheduler
import io.objectbox.android.ObjectBoxLiveData
import io.objectbox.query.Query
import io.realm.Realm

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val request = OneTimeWorkRequestBuilder<SeedDatabaseWorker>().build()
        WorkManager.getInstance(this).enqueue(request)

    }
}
