package com.example.sunflowerexam.utils

import android.app.Application
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import com.example.sunflowerexam.database.ObjectBox
import com.example.sunflowerexam.task.SeedDatabaseWorker
import io.realm.Realm
import io.realm.RealmConfiguration
import timber.log.Timber




class Application: Application() {
    companion object {
        const val TAG = "SUNFLOWER"
    }
    override fun onCreate() {
        super.onCreate()
        ObjectBox.init(this)
        Realm.init(this)
        val realmConfiguration = RealmConfiguration.Builder().name("db.realm").build()
        Realm.setDefaultConfiguration(realmConfiguration)

        Timber.plant(Timber.DebugTree())

    }


}