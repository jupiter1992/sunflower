package com.example.sunflowerexam.screen.garden


import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders

import com.example.sunflowerexam.R
import com.example.sunflowerexam.data.Plants
import com.example.sunflowerexam.databinding.FragmentGardenBinding
import com.example.sunflowerexam.databinding.FragmentPlantListBinding
import com.example.sunflowerexam.screen.plant.PlantListVM
import com.example.sunflowerexam.utils.Application
import io.realm.Realm

/**
 * A simple [Fragment] subclass.
 */
class GardenFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentGardenBinding.inflate(inflater,container,false)
        binding.hasGarden = false

        binding.lifecycleOwner = this

        return binding.root
    }


}
