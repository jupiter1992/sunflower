package com.example.sunflowerexam.screen.plant


import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController

import com.example.sunflowerexam.R
import com.example.sunflowerexam.data.Plant
import com.example.sunflowerexam.data.Plants
import com.example.sunflowerexam.database.ObjectBox
import com.example.sunflowerexam.databinding.FragmentPlantListBinding
import com.example.sunflowerexam.screen.home.HomePagerFragmentDirections
import com.example.sunflowerexam.utils.Application
import io.realm.Realm
import timber.log.Timber

/**
 * A simple [Fragment] subclass.
 */
class PlantListFragment : Fragment() {
    lateinit var plantListVM: PlantListVM
    lateinit var binding: FragmentPlantListBinding
    lateinit var clickDetaiListener: (Plants) -> Unit
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentPlantListBinding.inflate(layoutInflater,container,false)
        plantListVM = ViewModelProviders.of(this).get(PlantListVM ::class.java)
        binding.progressBar.visibility = View.VISIBLE
        val adapter = PlantListAdapter{
            findNavController().navigate(HomePagerFragmentDirections.actionHomePagerFragmentToDetailFragment(it))
        }

        binding.plantRecyclerView.adapter = adapter
        plantListVM.plants.observe(this, Observer {
            Timber.i(it.toString())
            it?.let {
                adapter.submitList(it)
                binding.progressBar.visibility = View.GONE
            }
        })


        return binding.root
    }


}
val itlist = listOf(Plants(imageUrl = "https://upload.wikimedia.org/wikipedia/commons/5/55/Apple_orchard_in_Tasmania.jpg"),
    Plants(imageUrl = "https://upload.wikimedia.org/wikipedia/commons/2/29/Beetroot_jm26647.jpg"),
    Plants(imageUrl = "https://upload.wikimedia.org/wikipedia/commons/5/51/A_scene_of_Coriander_leaves.JPG"),
    Plants(imageUrl = "https://upload.wikimedia.org/wikipedia/commons/1/17/Cherry_tomatoes_red_and_green_2009_16x9.jpg"),
    Plants(imageUrl = "https://upload.wikimedia.org/wikipedia/commons/e/e4/Branch_and_fruit_of_the_Maluma_avocado_cultivar.jpg"),
    Plants(imageUrl = "https://upload.wikimedia.org/wikipedia/commons/1/13/More_pears.jpg"),
    Plants(imageUrl = "https://upload.wikimedia.org/wikipedia/commons/e/e5/Eggplant_display.JPG"),
    Plants(imageUrl = "https://upload.wikimedia.org/wikipedia/commons/0/03/Grape_Plant_and_grapes9.jpg"),
    Plants(imageUrl = "https://upload.wikimedia.org/wikipedia/commons/6/67/Mangos_criollos_y_pera.JPG"),
    Plants(imageUrl = "https://upload.wikimedia.org/wikipedia/commons/2/22/Apfelsinenbaum--Orange_tree.jpg"),
    Plants(imageUrl = "https://upload.wikimedia.org/wikipedia/commons/a/aa/Sunflowers_in_field_flower.jpg"),
    Plants(imageUrl = "https://upload.wikimedia.org/wikipedia/commons/f/fc/01266jfWatermelons_Philippines_textures_Apolonio_Samson_Market_Quezon_Cityfvf_02.jpg"),
    Plants(imageUrl = "https://upload.wikimedia.org/wikipedia/commons/8/82/Hibiscus_rosa-sinensis_flower_2.JPG"),
    Plants(imageUrl = "https://upload.wikimedia.org/wikipedia/commons/a/ab/Cypripedium_reginae_Orchi_004.jpg"),
    Plants(imageUrl = "https://upload.wikimedia.org/wikipedia/commons/9/94/Aquilegia_caerulea.jpg"),
    Plants(imageUrl = "https://upload.wikimedia.org/wikipedia/commons/1/13/Yulan_magnolia_%28Magnolia_denudata%29_%2816953983745%29.jpg"),
    Plants(imageUrl = "https://upload.wikimedia.org/wikipedia/commons/6/6d/Paperflower_--_Bougainvillea_glabra.jpg"))