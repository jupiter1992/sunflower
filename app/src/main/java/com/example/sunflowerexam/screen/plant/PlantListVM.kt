package com.example.sunflowerexam.screen.plant

import androidx.lifecycle.*
import com.example.sunflowerexam.data.Plants

import io.realm.Realm
import io.realm.RealmChangeListener
import io.realm.RealmResults
import kotlinx.coroutines.*

class PlantListVM: ViewModel() {
    private val _plants = MutableLiveData<List<Plants>>()
    val plants: LiveData<List<Plants>>
    get() = _plants
    private lateinit var realm: Realm
    private val coroutineScope = CoroutineScope(Job() + Dispatchers.Main)
    private lateinit var  plantsResult: RealmResults<Plants>
    init {
        coroutineScope.launch {
            getPlants()

        }
    }

    private  fun getPlants() {
            realm = Realm.getDefaultInstance()
            plantsResult = realm.where(Plants ::class.java).findAllAsync()
            plantsResult.addChangeListener(RealmChangeListener {

                _plants.value = it

            })



    }



    override fun onCleared() {
        super.onCleared()
        coroutineScope.cancel()
        plantsResult.removeAllChangeListeners()
        realm.close()
    }






}