package com.example.sunflowerexam.screen.plant

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.sunflowerexam.data.Plants
import com.example.sunflowerexam.databinding.ListItemPlantBinding
import com.example.sunflowerexam.screen.home.HomePagerFragmentDirections

class PlantListAdapter(val onClickListener : (String) -> Unit): ListAdapter<Plants,PlantListItemViewHolder>(DiffUtilCallBack()) {
    companion object {
        class DiffUtilCallBack : DiffUtil.ItemCallback<Plants>() {
            override fun areItemsTheSame(oldItem: Plants, newItem: Plants): Boolean {
                return oldItem.plantId == newItem.plantId
            }

            override fun areContentsTheSame(oldItem: Plants, newItem: Plants): Boolean {
                return oldItem == newItem
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlantListItemViewHolder {
        return PlantListItemViewHolder(ListItemPlantBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }

    override fun onBindViewHolder(holder: PlantListItemViewHolder, position: Int) {
        val item = getItem(position)
        holder.binding(item)
        holder.itemView.setOnClickListener {
            item.plantId?.let {
                onClickListener(it)
            }
        }

    }

}
class PlantListItemViewHolder(private val binding: ListItemPlantBinding): RecyclerView.ViewHolder(binding.root) {

    fun binding(plants: Plants) {
        binding.plant = plants
        binding.executePendingBindings()


    }
}