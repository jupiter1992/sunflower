package com.example.sunflowerexam.screen.home


import android.graphics.drawable.Drawable
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController

import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.adapter.FragmentViewHolder
import com.example.sunflowerexam.R
import com.example.sunflowerexam.databinding.FragmentGardenBinding
import com.example.sunflowerexam.databinding.FragmentHomePagerBinding
import com.example.sunflowerexam.databinding.FragmentPlantListBinding
import com.example.sunflowerexam.screen.garden.GardenFragment
import com.example.sunflowerexam.screen.plant.PlantListFragment
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator



class HomePagerFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentHomePagerBinding.inflate(inflater,container,false)
        binding.viewPager.adapter = HomePageAdapter(this)
        TabLayoutMediator(binding.tabBar,binding.viewPager) { tab: TabLayout.Tab, i: Int ->
            tab.text = getTextTab(i)
            tab.setIcon(getIconTab(i))

        }.attach()
        (activity as AppCompatActivity).setSupportActionBar(binding.toolBar)
        binding.lifecycleOwner = this



        return binding.root
    }

    private fun getTextTab(position: Int): String {
        return if (position == HomePage.GARDEN.value) {
            getString(R.string.my_garden_title)
        } else {
            getString(R.string.plant_list_title)
        }
    }

    private fun getIconTab(position: Int): Int {
        return if (position == HomePage.GARDEN.value) {
            R.drawable.garden_tab_selector
        } else {
            R.drawable.plant_list_tab_selector
        }
    }


}
private enum class HomePage(val value: Int) {
    GARDEN( 0),
    PLANT_LIST(1)
}
class HomePagerAdapter: RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == HomePage.GARDEN.value) {
            GardenFragmentVH(FragmentGardenBinding.inflate(LayoutInflater.from(parent.context),parent,false))
        } else {
            PlantListFragmentVH(FragmentPlantListBinding.inflate(LayoutInflater.from(parent.context),parent,false))
        }
    }

    override fun getItemCount(): Int {
       return 2
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

    }

    override fun getItemViewType(position: Int): Int {
        return if (position == 0) {
            HomePage.GARDEN.value
        } else {
            HomePage.PLANT_LIST.value
        }
    }


}
class GardenFragmentVH(private val binding: FragmentGardenBinding): RecyclerView.ViewHolder(binding.root)
class PlantListFragmentVH(private val binding: FragmentPlantListBinding): RecyclerView.ViewHolder(binding.root)
class HomePageAdapter(fragment: Fragment): FragmentStateAdapter(fragment) {
    override fun getItemCount(): Int {
        return 2
    }

    override fun createFragment(position: Int): Fragment {
        return if (position == 0) {
            GardenFragment()
        } else {
            PlantListFragment()
        }
    }

    override fun onBindViewHolder(
        holder: FragmentViewHolder,
        position: Int,
        payloads: MutableList<Any>
    ) {
        super.onBindViewHolder(holder, position, payloads)
    }


}