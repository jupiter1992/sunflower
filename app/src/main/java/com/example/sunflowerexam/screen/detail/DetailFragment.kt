package com.example.sunflowerexam.screen.detail


import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.text.HtmlCompat
import androidx.core.widget.NestedScrollView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController

import com.example.sunflowerexam.R
import com.example.sunflowerexam.databinding.FragmentDetailBinding

/**
 * A simple [Fragment] subclass.
 */
class DetailFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentDetailBinding.inflate(inflater,container,false)
        binding.lifecycleOwner = this
        val plantId = DetailFragmentArgs.fromBundle(arguments!!).plantId
        val detailVM = ViewModelProviders.of(this).get(DetailVM ::class.java)
        detailVM.getPlant(plantId)
        detailVM.plants.observe(this, Observer {
            it?.let {
                binding.plant = it

            }
        })
        if (detailVM.plants.value?.description == null) {
            binding.plantDescription.text = ""
        }
        else {
            binding.plantDescription.text = HtmlCompat.fromHtml(detailVM.plants.value?.description!!, HtmlCompat.FROM_HTML_MODE_COMPACT)
            binding.plantDescription.movementMethod = LinkMovementMethod.getInstance()
        }
        binding.toolBar.setOnClickListener {
            findNavController().navigateUp()
        }
        var isToolbarShown = false
        binding.plantDetailScrollview.setOnScrollChangeListener(NestedScrollView.OnScrollChangeListener{_,_,scrollY,_,_ ->
            val shouldShowToolbar = scrollY > binding.toolBar.height

            // The new state of the toolbar differs from the previous state; update
            // appbar and toolbar attributes.
            if (isToolbarShown != shouldShowToolbar) {
                isToolbarShown = shouldShowToolbar

                // Use shadow animator to add elevation if toolbar is shown
                binding.appBar.isActivated = shouldShowToolbar

                // Show the plant name if toolbar is shown
                binding.toolBarLayout.isTitleEnabled = shouldShowToolbar
            }
        })


        return binding.root
    }


}
