package com.example.sunflowerexam.screen.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.example.sunflowerexam.data.Plants
import io.realm.Realm
import io.realm.RealmChangeListener
import io.realm.RealmResults
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class DetailVM: ViewModel() {
    private val coroutineScope = CoroutineScope(Job() + Dispatchers.Main)


    private val _plants = MutableLiveData<Plants>()

    val plants: LiveData<Plants>
        get() = _plants
    private val realm: Realm = Realm.getDefaultInstance()

     fun getPlant(plantId: String) {
        _plants.value =  realm.where(Plants ::class.java).equalTo("plantId",plantId).findFirst()


    }
}