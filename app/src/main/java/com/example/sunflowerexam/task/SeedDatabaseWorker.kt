package com.example.sunflowerexam.task

import android.content.Context
import com.google.gson.stream.JsonReader
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.example.sunflowerexam.data.Plants

import com.example.sunflowerexam.utils.PLANTS_JSON
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

import io.realm.Realm
import java.lang.Exception

import kotlinx.coroutines.*


class SeedDatabaseWorker(appContext: Context, params: WorkerParameters): CoroutineWorker(appContext,params) {
    override suspend fun doWork(): Result =
        withContext(Dispatchers.IO) {
            try {
                applicationContext.assets.open(PLANTS_JSON).use { inputStream ->
                    JsonReader(inputStream.reader()).use { jsonReader ->
                        val plantType = object : TypeToken<List<Plants>>() {}.type
                        val plantList: List<Plants> = Gson().fromJson(jsonReader,plantType)
                        val realm = Realm.getDefaultInstance()
                        realm.executeTransaction {
                            it.delete(Plants ::class.java)
                            plantList.forEach {plant ->

                                it.insertOrUpdate(plant)
                            }
                        }

                    }

                    Result.success()

                }
            }
            catch (e: Exception) {
                Result.failure()
            }

        }

}