package com.example.sunflowerexam.data

import io.objectbox.annotation.Entity
import io.objectbox.annotation.Id
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Entity
data class Plant(@Id  var id: Long , var plantId: String, var name: String, var description: String, var growZoneNumber: Int, var wateringInterval: Int, var imageUrl: String)

open  class Plants(@PrimaryKey var plantId: String? = null, var name: String? = null, var description: String? = null,
                  var growZoneNumber: Int? = null, var wateringInterval: Int? = null, var imageUrl: String? = null): RealmObject() {
    override fun equals(other: Any?): Boolean {
        return plantId == (other as Plants).plantId
    }

}